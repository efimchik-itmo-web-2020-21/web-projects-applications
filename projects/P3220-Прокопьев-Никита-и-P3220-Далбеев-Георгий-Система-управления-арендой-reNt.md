# Система управления арендой "reNt"

## Авторы
- P3220 Прокопьев Никита Вадимович
- P3220 Далбеев Георгий Георгиевич

## Описание
### Сервис позволит для арендодателя управлять арендой собственности, а именно:
- выбирать арендатора
- отслеживать сроки аренды
- обрабатывать заявки на аренду

### А клиентам:
- просмаривать каталог доступных для аренды мест и их цену
- создавать заявку на аренду

## Детали
- Продукт - веб-приложение с базой данных, пользовательских интерфейс - веб-страница.
- Стек технологий - Spring MVC, MySql, Thymeleaf и возможно другие по ходу разработки